﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DateTimeConverter
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow :  Window, IMultiValueConverter
    {
        public MainWindow()
        {
            
            InitializeComponent();
        }

        public int ConvertBack(DateTime date)
        {
            int newDate = Convert.ToInt32(date);
            return newDate;
        }

        public DateTime MultiValueConverter(int yearNumber, int monthNumber, int dayNumber)
        {
            DateTime date = new DateTime(yearNumber, monthNumber, dayNumber).Date;
            return date;
        }
        

        private void СonvertButton_Click(object sender, RoutedEventArgs e)
        {
            if (!(dateTextBox.ToString() == ""))
            {
                char point = '.';
                int indexOfPoint = dateTextBox.Text.ToString().IndexOf(point) + 1;
                string dayMonthString = dateTextBox.Text.ToString().Substring(indexOfPoint);
                int newIndexOfPoint = dayMonthString.IndexOf(point) + 1;
                string dayString = dayMonthString.Substring(newIndexOfPoint);
                string monthString = dayMonthString.Substring(0, newIndexOfPoint - 1);
                string yearString = dateTextBox.Text.ToString().Substring(0, indexOfPoint-1);
                int day = Convert.ToInt32(dayString);
                int month = Convert.ToInt32(monthString);
                int year = Convert.ToInt32(yearString);
                if (day != 0 && day <= 31 && month != 0 && month <= 12 && year != 0)
                {
                    DateTime newDate = MultiValueConverter(year, month, day);
                    newDateTextBox.Text = newDate.ToString();
                }
                else MessageBox.Show("Введена неверная дата!");
               
            }
        }
    }
}
