﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DateTimeConverter
{
   public interface IMultiValueConverter
    {
         DateTime MultiValueConverter(int yearNumber, int monthNumber, int dayNumber);
         int ConvertBack(DateTime date);
    }

}
